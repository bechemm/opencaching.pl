<?php

/**
  
 Skrypt naprawia blednie wyliczoną wysokość kesza nad poziomem morza.
 Wysokość jest obliczna w momencie ustawienia/zmiany współrzędnych.
 
 Skrypt:
 - znajduje kesze o braku bądź nietypowej (prawdopodobnie błednej) wysokości
 - dla każdego znalezionego kesza uruchamia procedurę wyliczenia wysokości
 
 Skrypt odpalamy w katalogu /shell, albo trzeba poprawić ścieżkę...
 
*/


$rootpath = '../';
require_once ('../lib/common.inc.php');

use \lib\Database\DataBaseSingleton;
use \lib\Objects\GeoCache;

const MIN_ALTITUDE = -50;
const MAX_ALTITUDE = 2500;

Altitude_fix::run();

class Altitude_fix
{
    
    static function run(){
        
        $query = "SELECT cache_id, altitude FROM caches_additions WHERE altitude < ".MIN_ALTITUDE." OR altitude > ".MAX_ALTITUDE." OR altitude=0";
        
        $db = DataBaseSingleton::Instance();
        $db->simpleQuery($query);
        $dbResult = $db->dbResultFetchAll();
        
        foreach($dbResult as $row){
            echo "Found cacheId: ".$row['cache_id']." with altitude ".$row['altitude']."\n";
            self::updateAltitude($row['cache_id']);            
        }
    }
    
    
    static function updateAltitude($id){
        $cache = new \lib\Objects\GeoCache\GeoCache( array('cacheId'=>$id) );
        $cache->getAltitude()->pickAndStoreAltitude(NULL);
        echo "CacheId: $id new altitude: ".$cache->getAltitude()->getAltitude()."\n";
    }
    
}

